# Week 1 - Git
May 13, 2019

## What is Git?

- By far one of the most widely used modern version control system in the today. It is open source and is developed in 2005 by Linus Torvalds (He also made the Linux operation system kernel, wow)
- It is important to know that Github != Git, Github simply uses Git. Some other platforms that use git are Bitbucket, VSTS, JIRA, etc.
- Git is a DVSC (Distributed Version Control System) meaning that EVERYONE has a copy of the whole repository.

## Why use Git?

- It allows for sharing and collaboration of code.
- You could also say emailing 1000 lines of code and passing a USB around the same thing, however Git has some **BONUS**
- Git has a history of all commits, so no longer do you have to seach through the trash can or spam ```ctrl + z```
- It has automatic merging which allows for developers to work on different areas of code without having to deal with synchronization.
- It's a standard and can be configured in ways to protect production code with PRs and branch policies

## Terms
| **Term** | **Definition** |
|------|----------------------------------|
| HEAD | The current snapshot of a branch |
| Fork | The process of creating a separate branch from another branch |
| Master | The default and main branch (Usually also the production branch, a branch you don't want to break) |
| Repository | A collection of commits and branches to identify commits |

## Common git commands

### ```git branch```
| **Command** | **Definition** |
|------|----------------------------------|
| ```git branch``` | Shows all the local branches |
| ```git branch -a``` | Shows all the local and remote branches |
| ```git branch [ name ]``` | Creates branch [ name ] |
| ```git branch -d [ name ]``` | Delete branch [ name ] |
| ```git branch -D [ name ]``` | Really delete branch [ name ] (Kind of like -rf) |

### ```git checkout```
| **Command** | **Definition** |
|-------------------------|----------------------------------|
| ```git checkout [ name ]``` | Check out branch [ name ] |
| ```git checkout -b [ name ]``` | Create branch [ name ] and check it out |
| ```git checkout -b [ name ] origin/[ other name ]``` | Create branch [ name ] which is a clone of branch [ other name ] from remote |

### ```git merge```
| **Command** | **Definition** |
|-------------------------|----------------------------------|
| ```git merge [ name ]``` | Merge branch [ name ] into your current branch |
| ```git merge [ from ] [ to ]``` | Merge branch [ from ] to branch [ to ] |

### ```git pull```
| **Command** | **Definition** |
|------|----------------------------------|
| ```git pull origin [ name ]``` | Pull changes from remote branch into current branch (Can be any branch) |
| ```git pull``` | Update local repo with newest |

### ```git push```
| **Command** | **Definition** |
|------|----------------------------------|
| ```git push``` | Push current branch into remembered branch |
| ```git push origin [ name ]``` | Push changes into remote branch [ name ] |
| ```git push -u origin [ name ]``` | Push changes into remote branch [ name ] (And remembers branch) |
| ```git push origin --delete [ name ]``` | Deletes remote branch [ name ] |

### ```git remote```
| **Command** | **Definition** |
|----------------------------------------|----------------------------------|
| ```git remote add origin [ ... ]``` | Set remote repository to [ ... ] |
| ```git remote add [ name ] [ ... ]``` | Set another remote repository to [ ... ] (Which you can access by changing origin to [ name ] for commands above) |

## Advice moving forward to project
- Please use branch policies to ensure production works
    - Ensure at least one other member reviews code in PR
    - Can't push directly into master, must make a PR to merge in
- Having a branch naming convention
    - This helps you keep track of what branch coresponds to what ticket
    - You can use hooks to parse the PR name and autoamtically move tickets in JIRA (Not sure the details, but glad to explore it when I get JIRA :p)
- Use CI/CD
    - Code gets tested so at the bare minimum. At least the application doesn't crash instantly
    - CD would allow for your to automatically deploy your application so you don't have to worry about it since we're lazy
    - My recommended CI platform is [TravisCI](https://travis-ci.org/)
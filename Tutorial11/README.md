# Singleton & Observer/Observable

## Singleton
 - Makes sure that there is only one instance of itself and is usually "global"
 - Can be initialized in runtime (or just-in-time) when needed
 - Usually used for things such as database connections
 - Made in two parts => **Singleton** & **Instance fetcher**
 
![Singleton](https://sourcemaking.com/files/v2/content/patterns/singleton1.png)
![Fetcher](https://sourcemaking.com/files/v2/content/patterns/Singleton.png)

### Key Points
 - You want the constructor to be private so that you can't create the Singleton outside itself
 - You want to store an instance of itself into itself O.o
 - You need a static method, getInstance, to get itself (or create itself if itself doesn't exist)
 

## Observer/Observable
 - You want some things to change when another thing changes (one to many relationship)
 - A good way to encapsulate core functionalities as the observable, and extend it to others with observers
 - Synchronization of objects or even allowing for "side-effects" or hooks to extend functionality
 
![Observer/Observable](https://sourcemaking.com/files/v2/content/patterns/Observer.png)

### Key Points
 - Note that just because something is an observer doesn't mean it can't be observable (The world is your oyster, live your best life)
 - Observable
    - Has a list of observers, where observers use a method to subscribe to the observable (aka add observer to list)
    - Has ways to get and set data
    - When a get is triggered, it would iterate through the list of observers to tell it something happened
        - This can be done in a few ways, it can just be an signal which observers would get the new data from, or the signal can also contain the new data
 - Observer
    - Subscribes to observable and keeps track of the observable instance
    - Has a method for observable to call when something happens
    - In that method, proceed to grab new data or do anything you want object to do (You are your own person)

![Feelsbadman](http://www.quickmeme.com/img/98/98d2795b917bcfb3249672bad615294bf0dee1240947560c1ae4ac8ed42eb2be.jpg)

## References
 - [https://sourcemaking.com/design_patterns/singleton](https://sourcemaking.com/design_patterns/singleton)
 - [https://sourcemaking.com/design_patterns/observer](https://sourcemaking.com/design_patterns/observer)
 - [Javascript singleton because it's kind hard since no private](https://www.dofactory.com/javascript/singleton-design-pattern)
 

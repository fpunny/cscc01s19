# DoggoLoggo
![Amazing Logo](https://i.ibb.co/n87Jdkz/export.png)
## Aside
 - These requirements are horrible because you are expected to **ASK QUESTIONS**
 - All stats are fake for the sake of roleplaying
 - Assume you can have anything needed to support your software, be it a fidget spinner to a quantum computer, whatever.
 
## Overview
DoggoLoggo is a startup founded in the 1930s with the goal of logging doggos. With a team of 21 strong, we strive to provide a world where no doggo is left unloggo, so that owners can be as ease when owning a doggo.
We currently provide services such as checkup reminders, well-being services, and data tracking. With 69% of all Canadians owning dogs, we are estiamted to have a net worth of 5 billion rupees.

## Want
Currently all our information about the dogs are stored in tapes, and are accessed using a turing machine inherited by our co-founder Alan Turing.
Often times our interns would accidently feed it into an enumerator, which results in lost data.
We hope that with the help of UTSC, we can create a modern solution to replace our existing system, and implement newer features with the help of **TECHNOLOGY** and **IBM WATSON**.

![Squidward](https://media.giphy.com/media/vbNWVFTw5RdRe/giphy.gif)
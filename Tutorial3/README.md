# User Requirements

# What and why?
 - User requirements are basically things that users want and need
 - Sometimes they are really bad
    - They can be unclear, missing important details
    - They can make little / no sense from a technological perspective
    - They can be non-existant (rip)
 - So why are requirements so bad?
    - Users are not developers, hence they find some details irrelevant
    - They don't know what they want
    - They lack the expertise to come up with better solutions

![User requirements meme](https://i.redd.it/iznqp1y5vs4z.jpg)

# What can we do to fix this?
 - **ASK QUESTIONS** (They don't bite, I promise... at least not yet >:])
 - Do iterations. It's better to fix the problem early on than to replace a whole system later
 - Don't be so hastly to throw something away. The client can easy turn around and what it back
 
# Personas & Users Stories
## Why?
 - You might think, "Fred! I just need to make a chatty boi, why do I need this?"
    - You're working in a team, hence use the team. Right now we are doing a simple project with 4 people since this is considered introductory.
    Once you do something like D01, you would have a team of 7 working for Matplotlib (Good luck winging that).
    - This is an application made for the client, not you. Therefore, what you need and want doesn't necesarily align with your client.
    - Therefore, doing this helps better divide work, align thoughts of everyone in the team, and put yourselves closer to the client's shoes

# Personas
 - Personas are fictional or mock users used to cover possible target users for an application.
 - They are derived from research into the client and cover the needs, experience, behaviours, and goals of target users
 - Here is what a typical user story looks like

## Persona Example (From when I did C01)
```
Anna
 - 22 years old, female.
 - Attending undergraduate school.
 - Have been volunteering for 4 months at the organization.
 - Familiar with current ICare template in use and excel.
 - Use Chrome to access the internet.
 - Familiar with both hand-held devices and computers.
 - Desire to learn new things and ability to learn it quickly.
 - Has basic programming skills.
 - Her task is to fill the iCare template with the client information.
 - Works onsite on her personal Windows laptop.
 - Often struggles to find the frequency of visits of the immigrants from the large sets of data.
```
### Key points to take home
 - You want to cover the demographics of the user and role in the application
 - You want to cover the person's ability as it would reflect how they might interact with the application
 - You want to cover any frustrations or obstacles faced by the user, (Or goals and wants)
 - Long story short, one or a few personas cover a certain group. And it's your job to access their ability, wants, and needs to best cater the application for them

# User Stories
 - A way to describe an action of a user for the application to get an idea of how the wants and needs of user relate to the application
 - They usually follow the format of **As [ name ], I [ anything along the lines of wanting something ] to [ do action that is feature ], such that [ why does this user want to do this? ]**
 - Be wary about what this action is because...
    - **"As Bob, I would like a responsive web application so that I can use it on my phone"** or
    - **"As Sabba, I want the web application to work on IE, so that I can use it on my 1990s Potatobook"**, is not a valid user story. (Remember kids, responsive first ;])
 - We want the user story to specific, and cover an area of an application rather than a characteristic of an application. Hence something like,
    - **"As Kevin, I would like to be able to login, so that I can access the dashboard of the application"** or
    - **"As Patrick, I would like to be able to register, so that I can have access to the application"** is valid
 - Usually each user story is a feature, **BUT** does not mean that it is a task
    - For example, the login user story can be split up into
       - Create database to store user login
       - Create endpoint to login
       - Create UI for login
       - Integrate back-end with front-end
 - If it's not a user story, doesn't mean you can't make a task for it. Someone needs to set everything up, or some little details need to be completed before features can be started on. So be it.

# Advice
 - Consider worse case senario. Define a MVP (Minimum viable product) so that atleast you have the core functionality. Then add as much features as time permits.
    - You would put all the features that go into the MVP into a ticket known as an EPIC
 - Depending on what you use, you can have different hierachies of ticktes such as EPIC -> Feature -> Task
    - It's good to have it all organized this way such that you know exactly what is being done, and for what
    - Also it gets really satisfying to see the progress made
 - Knowing everyone here is a student, when planning a burndown chart (Hopefully you guys learn this eventually).
 It's okay that everyday isn't consistent and you end up waterfalling when you're free. (Althought in real life, don't do this)

# Example Time!
I shall pretend to be a client >:)

![Evil client](https://orig00.deviantart.net/aa4a/f/2014/138/5/4/gif__log_horizon_creepy_shiroe_by_azael1332ragnarok-d7ivnqa.gif)
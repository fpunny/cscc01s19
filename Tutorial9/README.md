# UML

## What is UML?
 - Unified Modeling Language, which is used to model and document software
 - Can be used for development, aka defining classes and their relations. But can also be used for business processes
 - Developed around teh 1990s
 - For this assignment/course, we would focus on just the development aspect of it

![joke](https://i.redd.it/y0wptuzuqfn21.jpg)

## When should we make UML Diagrams?
 - Anytime! You can do it before as a plan, or even after as a form of documentation!
 - The first way is called "Forward Design" as he second is called "Backward Design"
 - We would focus on simply the Class Diagram since Java is all about those classes

## Notations
![Diagram](https://sourcemaking.com/files/sm/images/uml/img_124.jpg)

| Thing | Description |
| --- | --- |
| Class | The box that contains name and attributes |
| Class Name | The name of the Class - pls CamelCase |
| Attribute | The variables within it, you can go into more details such as define type and modifiers, or methods in a sub-section. |
| Generalization | Basically defines inheritance. The class of pointy end is the parent |
| Association | The n-to-n relationship of two classes in another class |
| Multiplicity | Defining the n of the n-to-n relationship |
| Aggregation | How many of a class is in the class, diamond side is the container |


## References
 - [https://tallyfy.com/uml-diagram/](https://tallyfy.com/uml-diagram/)
 - [https://sourcemaking.com/uml/modeling-it-systems/structural-view/class-diagram](https://sourcemaking.com/uml/modeling-it-systems/structural-view/class-diagram)
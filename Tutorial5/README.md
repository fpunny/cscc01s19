# Iterator Pattern
## Resource
 - [https://sourcemaking.com/design_patterns/iterator](https://sourcemaking.com/design_patterns/iterator)
 - "Fred why are you just taking us to a link? Why you no code?"
 - Well very simple explanation
    - This website actually does an extremely good job at explaining a wide range of patterns
    - It has diagrams for visual learners (WOW)
    - It has examples done in multiple languages (for those who don't like Java), with a comparasion of before and after
      - Everyone here should be 3rd/2nd year, I believe they should be able understand the logic from reading
    - I'm an undergrad student, this is a site focuses on design patterns only. I'm confident to say that they do a better job than I do
    - That being said, here is my interpretation of the reading in case you can't be bothered to read it (which you should, good stuff)
 - I will most likely pull all my notes from this for any design patterns in this course because that's just how good this thing is

## Iterator Pattern from an undergrad student
![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS8vZYTSw9-kNlIk4Y-0PGDlCo1CwEqYiIsUSUJ7Bq74NO8BV36)

### What is an Iterator?
 - Basically an abstraction for well iteratoring a set of objects
 - Its declarative, meaning you don't need to understand how it works. You just need to know what it has.
 - Some common examples of this is forEach for lists in JS or ArrayLists for the Java kids

### Why
 - Why do I need this? I have my for loops, my while loops, my do while loops, and most importantly my ReCuRsIoN!
    - This helps promote component-based programming, basically where a part/element of the code is encapsulated
    - Reduce configuration management because the component basically manages itself
    - Create a form of consistency across a range of objects (How iterators exist for different Lists in Java)
    - You don't expose the list itself

### Overview on how
![Diagram](https://sourcemaking.com/files/v2/content/patterns/Iterator.png)

 - Basically to pull this off, you need two pieces, Collection and Traversal.
   - Collection is the abstract wrapper that actually holds the objects together
   - Traversal is the thing that actually does the iterating
   
### Stuff straight from site that was really good to know
 - Usually should have first(), next(), is_done(), current_item()
   - Dont need exact names for example you can do is_done => hasNext, or current_item => item
 - Comparasion
    - [Code](https://sourcemaking.com/design_patterns/iterator/java/1)
    - Issues
        - Exposes the list for manipulation when iterating
        - Suppose you had to do multiple iterations, the user has to manage the state
        - The code is depedent on the collection implementation
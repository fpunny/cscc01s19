# Decorator & Composite

## Decorator

### Purpose
 - To add additional stuff to an object dynamically and flexibly relative to extending
 - This means that we can "extend" something in real time! Therefore, we can conditionally "update" an object via this
 - A really good example of this is basically Java Swing works

![Decorator UML](https://sourcemaking.com/files/v2/content/patterns/Decorator_.png)

### Notes
 - Note that you are still extending to pull this design pattern off. It's just that now you can dynamically combine objects to emulate creating a new extended object at once
 - Use interfaces to your advantage, by having different interfaces in certain places, you can create restrictions on what and how things can be decorated
     - For example, it makes sense to do ```new withPaint(new Car())```, but not the other way around
 - Decorators are usually one-to-one, meaning you wouldn't decorate two objects at once.


## Composite

### Purpose
 - To be able treat objects uniformly for objects in a tree
 - Any 1-to-many "has a" relationship
 - This allows for basically a cleaner and uniform way of performing an action

![Composite UML](https://sourcemaking.com/files/v2/content/patterns/Composite.png)

### Notes
 - Actions that you want all composite objects to have share an interface
 - Objects can have different actions. It's just that some should be shared and consistent.
 - To see whether or noy you've implemeneted this successfully. Your code that uses this shouldn't need to check for types and should work in loops
 
![meme](https://i.imgflip.com/2c9kgx.jpg)

## References
 - [https://sourcemaking.com/design_patterns/decorator](https://sourcemaking.com/design_patterns/decorator)
    - Most likely reason why there is no before and after because it requires you to make n! classes for n decorators
    - There is a c++ before and after if you're into that
 - [https://sourcemaking.com/design_patterns/composite](https://sourcemaking.com/design_patterns/composite)
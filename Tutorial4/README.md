# JIRA
This week is relatively short, we would simply go over JIRA and get started with some of the components to it.
Most of the content of this tutorial is demonstration based. But here is a small outline of what would be covered.

## What is JIRA?
 - A issue tracking system developed by Atlassian for bug tracking and agile project management
 - This is different from git
     - Git is the source control system, where it manages the code
     - JIRA is something more similar to Visual Studio Team Services (VSTS)
     - Github arguably is same with their project boards and insight, however not as powerful and complete. But with that said, JIRA is harder to use
     due to the fact that it is a complete package with many things

![JIRA joke](https://memegenerator.net/img/instances/80688824.jpg)

## How do I JIRA?
 - To access JIRA, the links can be found [here](https://cmsweb.utsc.utoronto.ca/jira/login.jsp) (Not sure if access has been given yet)
 - Every user should hae a project, many configurations such as columns and webhooks are restricted to JIRA administrators (Hence, don't worry about it)
 - This version of JIRA is slightly different from Atlassian because it is a local community version hosted by UTSC
     - Atlassian's proprietary version has more and looks amazing
 - If you are making a board, use scrum not kanban. Kanban doesn't have sprints.

## Overview
 - When at a board, there would be the following navigations items
     - Backlog
         - This is the list of all tickets (which encompasses all task, features, and epics) in the project
     - Active Sprints
         - A kanban board with all tickets in sprint backlog with 2 other columns for signifying the current status of the ticket
     - Releases
         - Triggers scheduled releases which can have automated builds and etc. Requires Bamboo from Atlassian
         - We don't have this, so let's assume this doesn't exist
     - Reports
         - Has information about sprints such as how many tasks are done in the sprint
         - Tracked via story points
     - Issues
         - Similar to backlog, however allows you to see details in a "gmail" like layout
     - Components
         - Another layer that sits on top of epics. It's simply a way to group certain tasks together for organizational purposes
         
## Types of tickets
- Story
    - User story goes here
- Task
    - Usually sits under a user story. This is all the things needed to be done for a story to be finished
- Bug
    - Any issues that arises throughout development
- Epic
    - Encompasses a bunch of user stories that complete a certain system such as login, backend, frontend, etc.
- Sub-task
    - Smaller than a task? I highly doubt we would get to this...
- In the end of the day, besides stories and tasks. How you use everything else is up to you

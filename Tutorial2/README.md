# Maven (And really almost any other package management system)

## What is a package management system (Let's call it PMS for short)?
 - A tools that automates the process of managing a range of programs / libraries for a system, be it your OS or application, in a consistent manner.
     - By managing, we mean the process of installing, upgrading, configuring, removing, and even a level of version controlling (More on this later)
     - Depending on the PMS, an application that uses a PMS is a package, or can be configured to be one, as well

## Why use PMS?
 - Dependencies are consistent and all in one place, hence application across multiple computers use the same stuff
 - More lean applications for sharing (ie. A React application, at least I have seen, can have up to 200MBs of library code. With PMS, we only need 1 file to represent that 200MB)
 - Saves time, effort, and is DRY
 
## Maven in a Nutshell
 - Maven is one of a few PMS for Java (Some other notable ones being ANT and Gradle)
 - Does a lot of other things from testing and building application, to deploying on library repository

### How to create Maven project
```
mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app
```
 - ```mvn``` is the Maven command which you can install with your favourite installer or PMS like apt and homebrew.
 - ```-DgroupId``` Same as the packages of Java. This also represents the organization that created the project
 - ```-DartifactId``` Is the folder that application is in. It is also the name of the package if on Maven repo
 - This is extremely dumbed down, but it should be enough to get the ball rolling
 
### What does it look like?
 - A typical Java application except now there is a funny file called POM.xml. This is a file that has information about your pacakge and the stuff it uses
 - Here is an example of a POM file
 
```
<project
   xmlns="http://maven.apache.org/POM/4.0.0"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
>
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.mycompany.app</groupId>
  <artifactId>my-app</artifactId>
  <packaging>jar</packaging>
  <version>1.0-SNAPSHOT</version>
  <name>Maven Quick Start Archetype</name>
  <url>http://maven.apache.org</url>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
</project>
```

### What else do I need to know?
 - ```mvn compile``` it builds the application so that now it has code from other packages imbeded in it. It is stored in the /target folder and more of less follows the same file structure as /src
 - ```mvn test``` it runs unit tests. It has to pass for Maven to build so make sure they work
 
## Fred, I NEED MORE PMS
 - Here are some other PMS that I quickly looked up for your enjoyment (With the exception of NPM which is just what I know mostly)

### NPM
 - Node.js equivalent, but slightly different
     - Has different dependencies to define when packages are needed and how they are used
     - No built in build / test, you literally have to make the script and setup build yourself (Webpack and Babel and stuff)
     - Structure is a little more loose. There are certain things you need, but other than that, you can have literally anything else.

### apt
 - Different from most in the way that your operating system is not a package (To no one's surprise)
 - Insanely powerful, from pointing to another package (VI -> VIM) to mapping windows to linux to an extent to run windows apps in realtime (WINE).
 
### Gradle
 - Another PMS extremely similar to Maven. One of the more notable usage is with Android and is that one thing the prof tells you not to touch in B07
 - Hopefully by the end of the semester, you would be comfortable enough with PMS such that you can tinker with it without blowing up your app
 - Uses .gradle which is basically JSON not really
 - Maven but you have to do a bit more such as build your own build script (So half Maven, half NPM?)
 - Apparently faster than Maven and runs without you installing Gradle (Cool beans)

## Word of advice for PMS
 - For any Python users who use pip. I highly recommend you read up on freezing your requirements because pip does a very bad job at documenting what packages it uses for an application.
     - Manually adding requirements work too, in fact its [apparently better](https://medium.com/@tomagee/pip-freeze-requirements-txt-considered-harmful-f0bce66cf895)
     - If you don't do it, you'll literally be the only one touching your application
 - I can imagine most people wanting to make Node application (Or Python with Django, which I say - god bless your soul) because its fast and what not. But I would also advice you perhaps otherwise
     - Node applications are single threaded and event based, hence is designed for handling many small task well.
     - When performing something heavy *cough* web scraping *cough*, you're application is going to hang and all your tasks would turn into a waterfall.
     - Therefore, consider other languages that support parallel processing such as Flask (Python), Sping (Java), or .NET (C# and why)

 